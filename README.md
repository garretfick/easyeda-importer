[![Build Status](https://travis-ci.org/garretfick/easyeda-importer.svg?branch=master)](https://travis-ci.org/garretfick/easyeda-importer)
[![Coverage Status](https://coveralls.io/repos/github/garretfick/easyeda-importer/badge.svg?branch=master)](https://coveralls.io/github/garretfick/easyeda-importer?branch=master)

Import KiCAD libraries and schematics into EasyEDA.

Run scripts in EasyEDA to import KiCAD documents into EasyEDA. You need to build the scripts
or use a release (in the future) before you can use directly in EasyEDA.

## Build

Run the following to build the source and generate a distribution file

`npm run dist`

Build output files are generated in `dist`.

